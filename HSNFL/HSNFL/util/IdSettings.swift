//
//  IdSettings.swift
//  HSNBA
//
//  Created by leobs on 10.03.18.
//  Copyright © 2018 leobs. All rights reserved.
//

import Foundation

class IdSettings{
    
    static var APPODEAL_ID = "ef431eebc57f915287811e7a647f9d3e160c5ab58a42c46b"
    static var ADMOB_ID_VIDEO = "ca-app-pub-8806065804623360/9283710612"
    static var ADMOB_ID_BANNER = "ca-app-pub-8806065804623360/7314230815"
    static var GOOGLE_ANALYTICS_ID = "UA-83861504-13"
    
    // Testers
  //  static var ADMOB_ID_VIDEO = "ca-app-pub-8806065804623360/2019927131"
  //  static var ADMOB_ID_BANNER = "ca-app-pub-8806065804623360/8997287079"
    
    init(){}
    
}
