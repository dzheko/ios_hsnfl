//
//  EndViewController.swift
//  HSNBA
//
//  Created by leobs on 07.03.18.
//  Copyright © 2018 leobs. All rights reserved.
//

import UIKit
import Appodeal
import GoogleMobileAds

class EndViewController : AnalyticsViewController,AppodealBannerViewDelegate,GADRewardBasedVideoAdDelegate{
    
    static var worldScore = 0
    static var highScore = 0
    static var userScore = 0
    static var numberOfAttempts = 0
    
    var keepGoingBtnClicked = false
    var adFullyWatched = false
    
    let iwBackground : UIImageView = {
        let iwBackground = UIImageView(image: #imageLiteral(resourceName: "slider_back_3"))
        iwBackground.translatesAutoresizingMaskIntoConstraints = false
        return iwBackground
    }()
    
    let twHighscore : UILabel = {
        let twHighscore = UILabel()
        twHighscore.text = "highscore : 30"
        twHighscore.translatesAutoresizingMaskIntoConstraints = false
        twHighscore.textColor = .white
        twHighscore.font = UIFont.boldSystemFont(ofSize: 18)
        twHighscore.textAlignment = .right
        twHighscore.backgroundColor = UIColor.clear
        return twHighscore
    }()
    
    let twWorldRecord : UILabel = {
        let twWorldRecord = UILabel()
        twWorldRecord.text = "world score : 30"
        twWorldRecord.translatesAutoresizingMaskIntoConstraints = false
        twWorldRecord.textColor = .white
        twWorldRecord.font = UIFont.boldSystemFont(ofSize: 18)
        twWorldRecord.textAlignment = .left
        twWorldRecord.backgroundColor = UIColor.clear
        return twWorldRecord
    }()
    
    let twYourScore : UILabel = {
        let twYourScore = UILabel()
        twYourScore.text = "You scored : "
        twYourScore.translatesAutoresizingMaskIntoConstraints = false
        twYourScore.textColor = .white
        twYourScore.font = UIFont.boldSystemFont(ofSize: 30)
        twYourScore.textAlignment = .center
        twYourScore.backgroundColor = UIColor.clear
        return twYourScore
    }()
    
    let twUserScore : UILabel = {
        let twUserScore = UILabel()
        twUserScore.text = "1"
        twUserScore.translatesAutoresizingMaskIntoConstraints = false
        twUserScore.textColor = .yellow
        twUserScore.font = UIFont.boldSystemFont(ofSize: 60)
        twUserScore.textAlignment = .center
        twUserScore.backgroundColor = UIColor.clear
        return twUserScore
    }()
    
    let twAverageText : UILabel = {
        let twAverageText = UILabel()
        twAverageText.text = "The average score is : 23 "
        twAverageText.translatesAutoresizingMaskIntoConstraints = false
        twAverageText.textColor = .white
        twAverageText.font = UIFont.boldSystemFont(ofSize: 20)
        twAverageText.textAlignment = .center
        twAverageText.backgroundColor = .clear
        return twAverageText
    }()
    
    let twMessageText : UILabel = {
        let twMessageText = UILabel()
        twMessageText.text = "Try again but focus this time"
        twMessageText.translatesAutoresizingMaskIntoConstraints = false
        twMessageText.textColor = .white
        twMessageText.font = UIFont.boldSystemFont(ofSize: 20)
        twMessageText.textAlignment = .center
        twMessageText.backgroundColor = .clear
        return twMessageText
    }()
    
    let playAgainBtn = UIButton()
    let keepGoingBtn = UIButton()
    
    let twKeepGoingText : UILabel = {
        let twKeepGoingText = UILabel()
        twKeepGoingText.text = "KEEP GOING?"
        twKeepGoingText.translatesAutoresizingMaskIntoConstraints = false
        twKeepGoingText.textColor = .white
        twKeepGoingText.font = UIFont.boldSystemFont(ofSize: 23)
        twKeepGoingText.textAlignment = .left
        twKeepGoingText.backgroundColor = .clear
        return twKeepGoingText
    }()
    
    let twPlayAgain = UILabel()
    var rewardBasedAd: GADRewardBasedVideoAd!

    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(iwBackground)
        view.addSubview(twHighscore)
        view.addSubview(twWorldRecord)
        view.addSubview(twYourScore)
        view.addSubview(twUserScore)
        view.addSubview(twAverageText)
        view.addSubview(twMessageText)
        view.addSubview(playAgainBtn)
        
        uiSetScores()
        
        //Appodeal.setTestingEnabled(true)
        
        let adTypes: AppodealAdType = [.MREC]
        Appodeal.initialize(withApiKey: IdSettings.APPODEAL_ID, types:  adTypes)
        
        let mrec = AppodealMRECView.init(rootViewController: self)!
        mrec.setDelegate(self);
        
        self.view.addSubview(mrec);
        view.addSubview(keepGoingBtn)
        
       
        mrec.translatesAutoresizingMaskIntoConstraints = false
        mrec.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        mrec.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        mrec.heightAnchor.constraint(equalToConstant: mrec.bounds.height).isActive = true
        mrec.topAnchor.constraint(equalTo: twMessageText.bottomAnchor, constant: 0).isActive = true
        
        createUiLayout()
        playAgainBtn.translatesAutoresizingMaskIntoConstraints = false
        playAgainBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        playAgainBtn.widthAnchor.constraint(equalToConstant: self.view.frame.size.width / 1.5).isActive = true
        playAgainBtn.heightAnchor.constraint(equalToConstant: self.view.frame.size.height / 12).isActive = true
        playAgainBtn.topAnchor.constraint(equalTo: mrec.bottomAnchor,constant: self.view.frame.size.height/20).isActive = true
        createKeepGoingBtn()
        
        if (EndViewController.numberOfAttempts >= 2) {
            keepGoingBtn.isHidden = true
            mrec.loadAd()
        } else if (EndViewController.userScore >= 3) {
            keepGoingBtn.isHidden = false
            rewardBasedAd = GADRewardBasedVideoAd.sharedInstance()
            rewardBasedAd.delegate = self
            rewardBasedAd.load(GADRequest(), withAdUnitID: IdSettings.ADMOB_ID_VIDEO)
        } else {
            keepGoingBtn.isHidden = true
            mrec.loadAd();
        }
        
     
        
    }

    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd,
                            didRewardUserWith reward: GADAdReward) {
        adFullyWatched = true
        print("Reward received with currency: \(reward.type), amount \(reward.amount).")
    }
    
    func rewardBasedVideoAdDidReceive(_ rewardBasedVideoAd:GADRewardBasedVideoAd) {
        if(keepGoingBtnClicked){
            rewardBasedAd.present(fromRootViewController: self)
        }
        print("Reward based video ad is received.")
    }
    
    func rewardBasedVideoAdDidOpen(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Opened reward based video ad.")
    }
    
    func rewardBasedVideoAdDidStartPlaying(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Reward based video ad started playing.")
    }
    
    func rewardBasedVideoAdDidClose(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        if(adFullyWatched){
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            ViewController.currentScore = EndViewController.userScore
            self.present(viewController, animated: true, completion: nil)
        }
        print("Reward based video ad is closed.")
    }
    
    func rewardBasedVideoAdWillLeaveApplication(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        print("Reward based video ad will leave application.")
    }
    
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd,
                            didFailToLoadWithError error: Error) {
        print("Reward based video ad failed to load.")
    }
    
    func uiSetScores(){
        twWorldRecord.text = "world score : " + String(EndViewController.worldScore)
        twHighscore.text = "highscore : " + String(EndViewController.highScore)
        twUserScore.text = String(EndViewController.userScore)
    }
    
   
    
    private func createUiLayout(){
        
        twHighscore.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        twHighscore.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true
        twHighscore.heightAnchor.constraint(equalToConstant: self.view.frame.size.height/10).isActive = true
        twHighscore.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        
        twWorldRecord.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10).isActive = true
        twWorldRecord.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        twWorldRecord.heightAnchor.constraint(equalToConstant: self.view.frame.size.height/10).isActive = true
        twWorldRecord.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        
        twYourScore.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        twYourScore.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        twYourScore.heightAnchor.constraint(equalToConstant: self.view.frame.size.height/12).isActive = true
        twYourScore.topAnchor.constraint(equalTo: twWorldRecord.bottomAnchor, constant: 0).isActive = true
        
        iwBackground.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        iwBackground.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        iwBackground.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        iwBackground.heightAnchor.constraint(equalToConstant: self.view.frame.size.height).isActive = true
        
        twUserScore.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        twUserScore.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        twUserScore.heightAnchor.constraint(equalToConstant: self.view.frame.size.height/12).isActive = true
        twUserScore.topAnchor.constraint(equalTo: twYourScore.bottomAnchor, constant: 0).isActive = true
        
        twAverageText.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        twAverageText.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        twAverageText.heightAnchor.constraint(equalToConstant: self.view.frame.size.height/12).isActive = true
        twAverageText.topAnchor.constraint(equalTo: twUserScore.bottomAnchor, constant: 0).isActive = true
        
        twMessageText.leftAnchor.constraint(equalTo: view.leftAnchor, constant: self.view.frame.size.width/12).isActive = true
        twMessageText.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -self.view.frame.size.width/12).isActive = true
        twMessageText.heightAnchor.constraint(equalToConstant: self.view.frame.size.height/24).isActive = true
        twMessageText.topAnchor.constraint(equalTo: twAverageText.bottomAnchor, constant: 0).isActive = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.playAgainClicked(_:)))
        playAgainBtn.addGestureRecognizer(tap)
        
        playAgainBtn.backgroundColor = .clear
        playAgainBtn.layer.cornerRadius = 10
        playAgainBtn.layer.borderWidth = 2
        playAgainBtn.layer.borderColor = UIColor.white.cgColor
        
        let reloadBtnImage = UIImageView(image:#imageLiteral(resourceName: "reload_btn"))
        playAgainBtn.addSubview(reloadBtnImage)
        
        reloadBtnImage.translatesAutoresizingMaskIntoConstraints = false
        reloadBtnImage.leftAnchor.constraint(equalTo: playAgainBtn.leftAnchor, constant: self.view.frame.size.width / 35).isActive = true
        reloadBtnImage.widthAnchor.constraint(equalToConstant: self.view.frame.size.height / 20).isActive = true
        reloadBtnImage.heightAnchor.constraint(equalToConstant: self.view.frame.size.height / 20).isActive = true
        reloadBtnImage.centerYAnchor.constraint(equalTo: playAgainBtn.centerYAnchor,constant: 0).isActive = true
        
        playAgainBtn.addSubview(twPlayAgain)
        twPlayAgain.translatesAutoresizingMaskIntoConstraints = false
        twPlayAgain.leftAnchor.constraint(equalTo: reloadBtnImage.rightAnchor, constant: self.view.frame.size.width / 15).isActive = true
        twPlayAgain.widthAnchor.constraint(equalToConstant: self.view.frame.size.width / 2).isActive = true
        twPlayAgain.heightAnchor.constraint(equalToConstant: self.view.frame.size.height/15).isActive = true
        twPlayAgain.centerYAnchor.constraint(equalTo: reloadBtnImage.centerYAnchor,constant: 0).isActive = true
        twPlayAgain.font = UIFont.boldSystemFont(ofSize: 23)
        twPlayAgain.textAlignment = .left
        twPlayAgain.textColor = UIColor.white
        twPlayAgain.text = "PLAY AGAIN"
        twPlayAgain.backgroundColor = UIColor.clear
    }
    
    func createKeepGoingBtn() {
        
        let keepGoingClicked = UITapGestureRecognizer(target: self, action: #selector(self.keepGoingBtnClicked(_:)))
        keepGoingBtn.addGestureRecognizer(keepGoingClicked)
        
        keepGoingBtn.backgroundColor = .clear
        keepGoingBtn.layer.cornerRadius = 10
        keepGoingBtn.layer.borderWidth = 2
        keepGoingBtn.layer.borderColor = UIColor.white.cgColor
        
        keepGoingBtn.translatesAutoresizingMaskIntoConstraints = false
        keepGoingBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        keepGoingBtn.widthAnchor.constraint(equalToConstant: self.view.frame.size.width / 1.5).isActive = true
        keepGoingBtn.heightAnchor.constraint(equalToConstant: self.view.frame.size.height / 12).isActive = true
        keepGoingBtn.bottomAnchor.constraint(equalTo: playAgainBtn.topAnchor,constant: -self.view.frame.size.height/20).isActive = true
        
        let videoPlayImage = UIImageView(image: #imageLiteral(resourceName: "ic_video_play"))
        keepGoingBtn.addSubview(videoPlayImage)
        videoPlayImage.translatesAutoresizingMaskIntoConstraints = false
        videoPlayImage.leftAnchor.constraint(equalTo: playAgainBtn.leftAnchor, constant: self.view.frame.size.width / 35).isActive = true
        videoPlayImage.widthAnchor.constraint(equalToConstant: self.view.frame.size.height / 20).isActive = true
        videoPlayImage.heightAnchor.constraint(equalToConstant: self.view.frame.size.height / 20).isActive = true
        videoPlayImage.centerYAnchor.constraint(equalTo: keepGoingBtn.centerYAnchor,constant: 0).isActive = true
        
        keepGoingBtn.addSubview(twKeepGoingText)
        twKeepGoingText.leftAnchor.constraint(equalTo: videoPlayImage.rightAnchor, constant: self.view.frame.size.width / 15).isActive = true
        twKeepGoingText.widthAnchor.constraint(equalToConstant: self.view.frame.size.width / 2).isActive = true
        twKeepGoingText.heightAnchor.constraint(equalToConstant: self.view.frame.size.height/15).isActive = true
        twKeepGoingText.centerYAnchor.constraint(equalTo: videoPlayImage.centerYAnchor,constant: 0).isActive = true
        
        
    }
    
    @objc func keepGoingBtnClicked(_ sender: UITapGestureRecognizer){
        sendEvent(category: "END VIEW BTNS", action: "KEEP GOING", label: "KEEP GOING")
        EndViewController.numberOfAttempts = EndViewController.numberOfAttempts + 1
        keepGoingBtnClicked = true
        if(rewardBasedAd.isReady){
            rewardBasedAd.present(fromRootViewController: self)
        }
    }
    
    @objc func playAgainClicked(_ sender: UITapGestureRecognizer) {
        sendEvent(category: "END VIEW BTNS", action: "PLAY AGAIN", label: "PLAY AGAIN")
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        ViewController.currentScore = 0
        EndViewController.numberOfAttempts = 0
        self.present(viewController, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
