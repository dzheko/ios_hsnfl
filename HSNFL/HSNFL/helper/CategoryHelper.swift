//
//
//  Category Helper class
//
//  @author Dzh
//  @since 06.03.2018
//

import Foundation
class CategoryHelper{
    
    init(){}
    
    /*
     * Returns category name
     * @param category
     * @return categoryFullName
     */
    static func getCategoryFullName(category: Int)->String{
        
        var categoryName = ""
        
        switch category {
        
        case 1:  categoryName = "passing attempts"; break;
        case 2:  categoryName = "passing attempts per game"; break;
        case 3:  categoryName = "passing completions"; break;
        case 4:  categoryName = "passing completions %"; break;
        case 5:  categoryName = "passing yards"; break;
        case 6:  categoryName = "passing yards per game"; break;
        case 7:  categoryName = "passing touchdowns"; break;
        
        case 8:  categoryName = "rushing attempts"; break;
        case 9:  categoryName = "rushing attempts per game"; break;
        case 10: categoryName = "rushing yards"; break;
        case 11: categoryName = "rushing yards per game"; break;
        case 12: categoryName = "rushing touchdowns"; break;
        case 13: categoryName = "rushing 100 yards rushgame"; break;
        
        case 14: categoryName = "receiving receptions"; break;
        case 15: categoryName = "receiving yards"; break;
        case 16: categoryName = "receiving yards per game"; break;
        case 17: categoryName = "receiving touchdowns"; break;
        case 18: categoryName = "receiving 25 yards"; break;
        case 19: categoryName = "receiving caught %"; break;
        
        case 20: categoryName = "kicking points"; break;
        case 21: categoryName = "kicking field goals"; break;
        case 22: categoryName = "kicking field goals attempts"; break;
        case 23: categoryName = "kicking field goals %"; break;
        case 24: categoryName = "kicking extra goals %"; break;
            
        case 25: categoryName = "defense total tackles"; break;
        case 26: categoryName = "defense solo tackles"; break;
        case 27: categoryName = "defense assisted tackles"; break;
        case 28: categoryName = "defense sacks"; break;
        case 29: categoryName = "defense sack yards"; break;
            
        case 30: categoryName = "scoring points"; break;
        case 31: categoryName = "scoring points per game"; break;

            
        default: categoryName = ""; break;
        }
        
        return categoryName
    }
    
    /*
     * Returns category name with TitleCase
     * @param category
     * @return categoryFullName
     */
    static func getCategoryFullNameTitleCase(category: Int)->String{
        
        var categoryName = ""
        
        switch category {
            
        case 1:  categoryName = "Passing attempts"; break;
        case 2:  categoryName = "Passing attempts per game"; break;
        case 3:  categoryName = "Passing completions"; break;
        case 4:  categoryName = "Passing completions %"; break;
        case 5:  categoryName = "Passing yards"; break;
        case 6:  categoryName = "Passing yards per game"; break;
        case 7:  categoryName = "Passing touchdowns"; break;
            
        case 8:  categoryName = "Rushing attempts"; break;
        case 9:  categoryName = "Rushing attempts per game"; break;
        case 10: categoryName = "Rushing yards"; break;
        case 11: categoryName = "Rushing yards per game"; break;
        case 12: categoryName = "Rushing touchdowns"; break;
        case 13: categoryName = "Rushing 100 yards rushgame"; break;
            
        case 14: categoryName = "Receiving receptions"; break;
        case 15: categoryName = "Receiving yards"; break;
        case 16: categoryName = "Receiving yards per game"; break;
        case 17: categoryName = "Receiving touchdowns"; break;
        case 18: categoryName = "Receiving 25 yards"; break;
        case 19: categoryName = "Receiving caught %"; break;
            
        case 20: categoryName = "Kicking points"; break;
        case 21: categoryName = "Kicking field goals"; break;
        case 22: categoryName = "Kicking field goals attempts"; break;
        case 23: categoryName = "Kicking field goals %"; break;
        case 24: categoryName = "Kicking extra goals %"; break;
            
        case 25: categoryName = "Defense total tackles"; break;
        case 26: categoryName = "Defense solo tackles"; break;
        case 27: categoryName = "Defense assisted tackles"; break;
        case 28: categoryName = "Defense sacks"; break;
        case 29: categoryName = "Defense sack yards"; break;
            
        case 30: categoryName = "Scoring points"; break;
        case 31: categoryName = "Scoring points per game"; break;
            
        default: categoryName = ""; break;
        }
        
        return categoryName
    }
    
    
    /*
     * Returns category name with TitleCase
     * @param category
     * @return categoryFullName
     */
    static func getCategoryQuestion(category: Int)->String{
        
        var categoryQuestion = ""
        
        switch category {
            
        case 1:  categoryQuestion = "Who has more passing attempts?"; break;
        case 2:  categoryQuestion = "Who has more attempts per game?"; break;
        case 3:  categoryQuestion = "Who has more passing completions?"; break;
        case 4:  categoryQuestion = "Who has higher passing completions %?"; break;
        case 5:  categoryQuestion = "Who has more passing yards?"; break;
        case 6:  categoryQuestion = "Who has more yards per game?"; break;
        case 7:  categoryQuestion = "Who has more passing touchdowns?"; break;
            
        case 8:  categoryQuestion = "Who has more rushing attempts?"; break;
        case 9:  categoryQuestion = "Who has more attempts per game?"; break;
        case 10: categoryQuestion = "Who has more rushing yards?"; break;
        case 11: categoryQuestion = "Who has more rushing yards per game?"; break;
        case 12: categoryQuestion = "Who has more rushing touchdowns?"; break;
        case 13: categoryQuestion = "Who has more rushing 100 yards rushgame?"; break;
            
        case 14: categoryQuestion = "Who has more receiving receptions?"; break;
        case 15: categoryQuestion = "Who has more receiving yards?"; break;
        case 16: categoryQuestion = "Who has more receiving yards per game?"; break;
        case 17: categoryQuestion = "Who has more receiving touchdowns?"; break;
        case 18: categoryQuestion = "Who has more receiving 25 yards?"; break;
        case 19: categoryQuestion = "Who has higher receiving caught %?"; break;
            
        case 20: categoryQuestion = "Who has more kicking points?"; break;
        case 21: categoryQuestion = "Who has more kicking field goals?"; break;
        case 22: categoryQuestion = "Who has more field goals attempts?"; break;
        case 23: categoryQuestion = "Who has higher field goals %?"; break;
        case 24: categoryQuestion = "Who has higher extra goals %?"; break;
            
        case 25: categoryQuestion = "Who has more defense total tackles?"; break;
        case 26: categoryQuestion = "Who has more defense solo tackles?"; break;
        case 27: categoryQuestion = "Who has more defense assisted tackles?"; break;
        case 28: categoryQuestion = "Who has more defense sacks?"; break;
        case 29: categoryQuestion = "Who has more defense sack yards?"; break;
            
        case 30: categoryQuestion = "Who has more scoring points?"; break;
        case 31: categoryQuestion = "Who has more points per game?"; break;
        default: categoryQuestion = ""; break;
        }
        
        return categoryQuestion
    }
    
    
    
    
    
}
