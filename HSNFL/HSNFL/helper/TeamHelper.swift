//
// Teamhelper class
//
//  @author Dzh
//  @since 06.03.2018
//
import Foundation

class TeamHelper{
    init(){}
    
    /*
     * Returns team image file
     * @param teamName
     * @return imageName
     */
    static func getTeamImage(teamName: String) -> String {
        
        var imageName = ""
        
    switch teamName {
        case "76ers":                 imageName = "img_philadelphia"; break;
        case "Wizards":               imageName = "img_washington"; break;
        case "Grizzlies":             imageName = "img_memphis"; break;
        case "Bucks":                 imageName = "img_milwaukee"; break;
        case "Chicago Bulls":         imageName = "img_chicago"; break;
        case "Cavaliers":             imageName = "img_cleveland"; break;
        case "Boston Celtics":        imageName = "img_boston"; break;
        case "LA Clippers":           imageName = "img_lac"; break;
        case "GS Warriors":           imageName = "img_gsw"; break;
        case "Atlanta Hawks":         imageName = "img_atalanta"; break;
        case "Miami Heat":            imageName = "img_miami"; break;
        case "Hornets":               imageName = "img_charlotta"; break;
        case "Utah Jazz":             imageName = "img_utah"; break;
        case "Sacramento":            imageName = "img_sacramento"; break;
        case "NY Knicks":             imageName = "img_nyk"; break;
        case "LA Lakers":             imageName = "img_lal"; break;
        case "Orlando Magic":         imageName = "img_orlando"; break;
        case "Mavericks":             imageName = "img_dallas"; break;
        case "Brooklyn Nets":         imageName = "img_brooklyn"; break;
        case "Denver Nuggets":        imageName = "img_denver"; break;
        case "Indiana Pacers":        imageName = "img_indiana"; break;
        case "NL Pelicans":           imageName = "img_orlean"; break;
        case "Pistons":               imageName = "img_detroit"; break;
        case "Raptors":               imageName = "img_toronto"; break;
        case "Rockets":               imageName = "img_houston"; break;
        case "SA Spurs":              imageName = "img_spurs"; break;
        case "Phoenix Suns":          imageName = "img_phoenix"; break;
        case "OKC Thunder":           imageName = "img_okc"; break;
        case "Timberwolves":          imageName = "img_minnesota"; break;
        case "Trail Blazers":         imageName = "img_portland"; break;
        default:                      imageName = "league_all"; break;
    }

        return imageName;
    }
    
    /*
     * Retuns Team Full Name
     */
    static func getTeamFullName(teamName : String)-> String{
        var imageName = ""
        var teamFullName = ""
        switch (teamName) {
        case "BAL":
            imageName = "nfl_baltimore";
            teamFullName = "BAL Ravens";
            break;
        case "IND":
            imageName = "nfl_indianapolis";
            teamFullName = "IND Colts";
            break;
        case "GB":
            imageName = "nfl_green_bay";
            teamFullName = "GB Packers";
            break;
        case "MIN":
            imageName = "nfl_minnesota";
            teamFullName = "MIN Vikings";
            break;
        case "CAR":
            imageName = "nfl_carolina";
            teamFullName = "CAR Panthers";
            break;
        case "TB":
            imageName = "nfl_tamba_bay";
            teamFullName = "TB Bucaneers";
            break;
        case "CHI":
            imageName = "nfl_chicago";
            teamFullName = "CHI Bears";
            break;
        case "CLE":
            imageName = "nfl_cleveland";
            teamFullName = "CLE Browns";
            break;
        case "CIN":
            imageName = "nfl_cincinnati";
            teamFullName = "CIN Bengals";
            break;
        case "DET":
            imageName = "nfl_detroit";
            teamFullName = "DET Lions";
            break;
        case "KC":
            imageName = "nfl_kansas";
            teamFullName = "Kansas City";
            break;
        case "MIA":
            imageName = "nfl_miami";
            teamFullName = "MIA Dolphins";
            break;
        case "NE":
            imageName = "nfl_new_england";
            teamFullName = "NE Patriots";
            break;
        case "BUF":
            imageName = "nfl_buffalo";
            teamFullName = "BUF Bills";
            break;
        case "NO":
            imageName = "nfl_new_orlean";
            teamFullName = "NE Saints";
            break;
        case "ATL":
            imageName = "nfl_atalanta";
            teamFullName = "ATL Falcons";
            break;
        case "NYJ":
            imageName = "nfl_ny_jets";
            teamFullName = "NY Jets";
            break;
        case "LAC":
            imageName = "nfl_la_chargers";
            teamFullName = "LA Chargers";
            break;
        case "TEN":
            imageName = "nfl_tennesse";
            teamFullName = "TEN Titans";
            break;
        case "LAR":
            imageName = "nfl_la_rams";
            teamFullName = "LA Rams";
            break;
        case "WSH":
            imageName = "nfl_washington";
            teamFullName = "WSH Redskins";
            break;
        case "DEN":
            imageName = "nfl_denver";
            teamFullName = "DEN Brocons";
            break;
        case "SF":
            imageName = "nfl_san_francisco";
            teamFullName = "SF 49ers";
            break;
        case "JAX":
            imageName = "nfl_jacksonville";
            teamFullName = "JAX Jaguars";
            break;
        case "ARZ":
            imageName = "nfl_arizona";
            teamFullName = "Cardinals";
            break;
        case "NYG":
            imageName = "nfl_ny_giants";
            teamFullName = "NY Giants";
            break;
        case "DAL":
            imageName = "nfl_dallas";
            teamFullName = "DAL Cowboys";
            break;
        case "SEA":
            imageName = "nfl_seatle";
            teamFullName = "Seahawks";
            break;
        case "HOU":
            imageName = "nfl_houston";
            teamFullName = "HOU Texans";
            break;
        case "PIT":
            imageName = "nfl_pittsburg";
            teamFullName = "PIT Steelers";
            break;
        case "PHI":
            imageName = "nfl_philadelphia";
            teamFullName = "PHI Eagles";
            break;
        case "OAK":
            imageName = "nfl_oakland";
            teamFullName = "OAK Raiders";
            break;
        default : teamFullName = ""; break;
        }
        
        return teamFullName;
    }
}
