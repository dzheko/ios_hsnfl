//
//
// DAO Object for Player
//  @author Dzh
//  @since 06.03.2018
//

import Foundation

class PlayerDAO{
    
    //Passing 1-50
    let CATEGORY1_ = "PASSING_ATTEMPTS_"
    let CATEGORY2_ = "PASSING_ATTEMPTS_PER_GAME_"
    let CATEGORY3_ = "PASSING_COMPLETIONS_"
    let CATEGORY4_ = "PASSING_COMPLETIONS_PERCENTAGE_"
    let CATEGORY5_ = "PASSING_YARDS_"
    let CATEGORY6_ = "PASSING_YARDS_PER_GAME_"
    let CATEGORY7_ = "PASSING_TOUCHDOWNS_"
    
    //Rushing 1-50
    let CATEGORY8_ = "RUSHING_ATTEMPTS_"
    let CATEGORY9_ = "RUSHING_ATTEMPTS_PER_GAME_"
    let CATEGORY10_ = "RUSHING_YARDS_"
    let CATEGORY11_ = "RUSHING_YARDS_PER_GAME_"
    let CATEGORY12_ = "RUSHING_TOUCHDOWNS_"
    let CATEGORY13_ = "RUSHING_100_YARDS_RUSHGAME_"
    
    //Receiving 1-50
    let CATEGORY14_ = "RECEIVING_RECEPTIONS_"
    let CATEGORY15_ = "RECEIVING_YARDS_"
    let CATEGORY16_ = "RECEIVING_YARDS_PER_GAME_"
    let CATEGORY17_ = "RECEIVING_TOUCHDOWNS_"
    let CATEGORY18_ = "RECEIVING_25_YARDS_"
    let CATEGORY19_ = "RECEIVING_CAUGHT_PERCENTAGE_"

    //Kicking 1-50
    let CATEGORY20_ = "KICKING_POINTS_"
    let CATEGORY21_ = "KICKING_FIELD_GOALS_"
    let CATEGORY22_ = "KICKING_FIELD_GOALS_ATTEMPTS_"
    let CATEGORY23_ = "KICKING_FIELD_GOALS_PERCENTAGE_"
    let CATEGORY24_ = "KICKING_EXTRA_GOALS_PERCENTAGE"
    
    //Defense 3-52
    let CATEGORY25_ = "DEFENSE_TOTAL_TACKLES_"
    let CATEGORY26_ = "DEFENSE_SOLO_TACKLES_"
    let CATEGORY27_ = "DEFENSE_ASSISTED_TACKLES_"
    let CATEGORY28_ = "DEFENSE_SACKS_"
    let CATEGORY29_ = "DEFENSE_SACK_YARDS_"
    
    //Scoring 3-52
    let CATEGORY30_ = "SCORING_POINTS_"
    let CATEGORY31_ = "SCORING_POINTS_PER_GAME_"
    
    
    init(){}
    
    /**
     * Creates Player
     */
    public func getPlayerData(categoryToPlayNumber : Int) -> Player {
        
        var player = Player(playerName: "", teamName: "")
        
        if (categoryToPlayNumber >= 1 && categoryToPlayNumber <= 7) {
            player = createPassingPlayer(categoryToPlayNumber : categoryToPlayNumber)
        } else if (categoryToPlayNumber >= 8 && categoryToPlayNumber <= 13) {
            player = createRushingPlayer(categoryToPlayNumber : categoryToPlayNumber)
        } else if (categoryToPlayNumber >= 14 && categoryToPlayNumber <= 19) {
            player = createReceivingPlayer(categoryToPlayNumber : categoryToPlayNumber)
        } else if (categoryToPlayNumber >= 20 && categoryToPlayNumber <= 24) {
            player = createKickingPlayer(categoryToPlayNumber : categoryToPlayNumber)
        } else if (categoryToPlayNumber >= 25 && categoryToPlayNumber <= 29) {
            player = createDefensePlayer(categoryToPlayNumber : categoryToPlayNumber)
        } else if (categoryToPlayNumber >= 30 && categoryToPlayNumber <= 31) {
            player = createScoringPlayer(categoryToPlayNumber : categoryToPlayNumber)
        }
        
        return player
    }
    
    /**
     * Creates Passing Player
     */
    private func createPassingPlayer(categoryToPlayNumber : Int) -> Player{
        let randomNumber =  arc4random_uniform(49) + 1
        let PLAYER_NAME = "PASSING_PLAYER_NAME_" + String(randomNumber)
        let PLAYER_TEAM = "PASSING_TEAM_NAME_" + String(randomNumber)
        let CATEGORY1 =  CATEGORY1_ + String(randomNumber)
        let CATEGORY2 =  CATEGORY2_ + String(randomNumber)
        let CATEGORY3 =  CATEGORY3_ + String(randomNumber)
        let CATEGORY4 =  CATEGORY4_ + String(randomNumber)
        let CATEGORY5 =  CATEGORY5_ + String(randomNumber)
        let CATEGORY6 =  CATEGORY6_ + String(randomNumber)
        let CATEGORY7 =  CATEGORY7_ + String(randomNumber)
        
        let playerTeam = NSLocalizedString(PLAYER_TEAM, comment: "")
        let playerName = NSLocalizedString(PLAYER_NAME, comment: "")
        let category1Stat = Double(NSLocalizedString(CATEGORY1, comment: ""))
        let category2Stat = Double(NSLocalizedString(CATEGORY2, comment: ""))
        let category3Stat = Double(NSLocalizedString(CATEGORY3, comment: ""))
        let category4Stat = Double(NSLocalizedString(CATEGORY4, comment: ""))
        let category5Stat = Double(NSLocalizedString(CATEGORY5, comment: ""))
        let category6Stat = Double(NSLocalizedString(CATEGORY6, comment: ""))
        let category7Stat = Double(NSLocalizedString(CATEGORY7, comment: ""))
        
        let player = Player(playerName: playerName, teamName: playerTeam)
        
        switch categoryToPlayNumber {
        case 1:  player.playerCategoryStatToPlay = category1Stat; break;
        case 2:  player.playerCategoryStatToPlay = category2Stat; break;
        case 3:  player.playerCategoryStatToPlay = category3Stat; break;
        case 4:  player.playerCategoryStatToPlay = category4Stat; break;
        case 5:  player.playerCategoryStatToPlay = category5Stat; break;
        case 6:  player.playerCategoryStatToPlay = category6Stat; break;
        case 7:  player.playerCategoryStatToPlay = category7Stat; break;
        default: player.playerCategoryStatToPlay = 0; break;
        }
        return player
    }
    
    
    /**
     * Creates Rushing Player
     */
    private func createRushingPlayer(categoryToPlayNumber : Int) -> Player{
        let randomNumber =  arc4random_uniform(49) + 1
        let PLAYER_NAME = "RUSHING_PLAYER_NAME_" + String(randomNumber)
        let PLAYER_TEAM = "RUSHING_TEAM_NAME_" + String(randomNumber)
        let CATEGORY8 =  CATEGORY8_ + String(randomNumber)
        let CATEGORY9 =  CATEGORY9_ + String(randomNumber)
        let CATEGORY10 =  CATEGORY10_ + String(randomNumber)
        let CATEGORY11 =  CATEGORY11_ + String(randomNumber)
        let CATEGORY12 =  CATEGORY12_ + String(randomNumber)
        let CATEGORY13 =  CATEGORY13_ + String(randomNumber)
        
        let playerTeam = NSLocalizedString(PLAYER_TEAM, comment: "")
        let playerName = NSLocalizedString(PLAYER_NAME, comment: "")
        let category1Stat = Double(NSLocalizedString(CATEGORY8, comment: ""))
        let category2Stat = Double(NSLocalizedString(CATEGORY9, comment: ""))
        let category3Stat = Double(NSLocalizedString(CATEGORY10, comment: ""))
        let category4Stat = Double(NSLocalizedString(CATEGORY11, comment: ""))
        let category5Stat = Double(NSLocalizedString(CATEGORY12, comment: ""))
        let category6Stat = Double(NSLocalizedString(CATEGORY13, comment: ""))
     
        let player = Player(playerName: playerName, teamName: playerTeam)
        
        switch categoryToPlayNumber {
        case 8:  player.playerCategoryStatToPlay = category1Stat; break;
        case 9:  player.playerCategoryStatToPlay = category2Stat; break;
        case 10:  player.playerCategoryStatToPlay = category3Stat; break;
        case 11:  player.playerCategoryStatToPlay = category4Stat; break;
        case 12:  player.playerCategoryStatToPlay = category5Stat; break;
        case 13:  player.playerCategoryStatToPlay = category6Stat; break;
        default: player.playerCategoryStatToPlay = 0; break;
        }
        return player
    }
    
    
    /**
     * Creates Receiving Player
     */
    private func createReceivingPlayer(categoryToPlayNumber : Int) -> Player{
        let randomNumber =  arc4random_uniform(49) + 1
        let PLAYER_NAME = "RECEIVING_PLAYER_NAME_" + String(randomNumber)
        let PLAYER_TEAM = "RECEIVING_TEAM_NAME_" + String(randomNumber)
        let CATEGORY14 =  CATEGORY14_ + String(randomNumber)
        let CATEGORY15 =  CATEGORY15_ + String(randomNumber)
        let CATEGORY16 =  CATEGORY16_ + String(randomNumber)
        let CATEGORY17 =  CATEGORY17_ + String(randomNumber)
        let CATEGORY18 =  CATEGORY18_ + String(randomNumber)
        let CATEGORY19 =  CATEGORY19_ + String(randomNumber)
        
        let playerTeam = NSLocalizedString(PLAYER_TEAM, comment: "")
        let playerName = NSLocalizedString(PLAYER_NAME, comment: "")
        let category1Stat = Double(NSLocalizedString(CATEGORY14, comment: ""))
        let category2Stat = Double(NSLocalizedString(CATEGORY15, comment: ""))
        let category3Stat = Double(NSLocalizedString(CATEGORY16, comment: ""))
        let category4Stat = Double(NSLocalizedString(CATEGORY17, comment: ""))
        let category5Stat = Double(NSLocalizedString(CATEGORY18, comment: ""))
        let category6Stat = Double(NSLocalizedString(CATEGORY19, comment: ""))
        
        let player = Player(playerName: playerName, teamName: playerTeam)
        
        switch categoryToPlayNumber {
        case 14:  player.playerCategoryStatToPlay = category1Stat; break;
        case 15:  player.playerCategoryStatToPlay = category2Stat; break;
        case 16:  player.playerCategoryStatToPlay = category3Stat; break;
        case 17:  player.playerCategoryStatToPlay = category4Stat; break;
        case 18:  player.playerCategoryStatToPlay = category5Stat; break;
        case 19:  player.playerCategoryStatToPlay = category6Stat; break;
        default: player.playerCategoryStatToPlay = 0; break;
        }
        return player
    }
    
   
    /**
     * Creates Kicking Player
     */
    private func createKickingPlayer(categoryToPlayNumber : Int) -> Player{
        let randomNumber =  arc4random_uniform(49) + 1
        let PLAYER_NAME = "KICKING_PLAYER_NAME_" + String(randomNumber)
        let PLAYER_TEAM = "KICKING_TEAM_NAME_" + String(randomNumber)
        let CATEGORY20 =  CATEGORY20_ + String(randomNumber)
        let CATEGORY21 =  CATEGORY21_ + String(randomNumber)
        let CATEGORY22 =  CATEGORY22_ + String(randomNumber)
        let CATEGORY23 =  CATEGORY23_ + String(randomNumber)
        let CATEGORY24 =  CATEGORY24_ + String(randomNumber)
        
        let playerTeam = NSLocalizedString(PLAYER_TEAM, comment: "")
        let playerName = NSLocalizedString(PLAYER_NAME, comment: "")
        let category1Stat = Double(NSLocalizedString(CATEGORY20, comment: ""))
        let category2Stat = Double(NSLocalizedString(CATEGORY21, comment: ""))
        let category3Stat = Double(NSLocalizedString(CATEGORY22, comment: ""))
        let category4Stat = Double(NSLocalizedString(CATEGORY23, comment: ""))
        let category5Stat = Double(NSLocalizedString(CATEGORY24, comment: ""))
        
        let player = Player(playerName: playerName, teamName: playerTeam)
        
        switch categoryToPlayNumber {
        case 20:  player.playerCategoryStatToPlay = category1Stat; break;
        case 21:  player.playerCategoryStatToPlay = category2Stat; break;
        case 22:  player.playerCategoryStatToPlay = category3Stat; break;
        case 23:  player.playerCategoryStatToPlay = category4Stat; break;
        case 24:  player.playerCategoryStatToPlay = category5Stat; break;
        default: player.playerCategoryStatToPlay = 0; break;
        }
        return player
    }
    
    
    /**
     * Creates Defense Player
     */
    private func createDefensePlayer(categoryToPlayNumber : Int) -> Player{
        let randomNumber =  arc4random_uniform(45) + 5
        let PLAYER_NAME = "DEFENSE_PLAYER_NAME_" + String(randomNumber)
        let PLAYER_TEAM = "DEFENSE_TEAM_NAME_" + String(randomNumber)
        let CATEGORY25 =  CATEGORY25_ + String(randomNumber)
        let CATEGORY26 =  CATEGORY26_ + String(randomNumber)
        let CATEGORY27 =  CATEGORY27_ + String(randomNumber)
        let CATEGORY28 =  CATEGORY28_ + String(randomNumber)
        let CATEGORY29 =  CATEGORY29_ + String(randomNumber)
        
        let playerTeam = NSLocalizedString(PLAYER_TEAM, comment: "")
        let playerName = NSLocalizedString(PLAYER_NAME, comment: "")
        let category1Stat = Double(NSLocalizedString(CATEGORY25, comment: ""))
        let category2Stat = Double(NSLocalizedString(CATEGORY26, comment: ""))
        let category3Stat = Double(NSLocalizedString(CATEGORY27, comment: ""))
        let category4Stat = Double(NSLocalizedString(CATEGORY28, comment: ""))
        let category5Stat = Double(NSLocalizedString(CATEGORY29, comment: ""))
        
        let player = Player(playerName: playerName, teamName: playerTeam)
        
        switch categoryToPlayNumber {
        case 25:  player.playerCategoryStatToPlay = category1Stat; break;
        case 26:  player.playerCategoryStatToPlay = category2Stat; break;
        case 27:  player.playerCategoryStatToPlay = category3Stat; break;
        case 28:  player.playerCategoryStatToPlay = category4Stat; break;
        case 29:  player.playerCategoryStatToPlay = category5Stat; break;
        default: player.playerCategoryStatToPlay = 0; break;
        }
        return player
    }
    
    /**
     * Creates Scoring Player
     */
    private func createScoringPlayer(categoryToPlayNumber : Int) -> Player{
        let randomNumber =  arc4random_uniform(50) + 5
        let PLAYER_NAME = "SCORING_PLAYER_NAME_" + String(randomNumber)
        let PLAYER_TEAM = "SCORING_TEAM_NAME_" + String(randomNumber)
        let CATEGORY30 =  CATEGORY30_ + String(randomNumber)
        let CATEGORY31 =  CATEGORY31_ + String(randomNumber)
       
        let playerTeam = NSLocalizedString(PLAYER_TEAM, comment: "")
        let playerName = NSLocalizedString(PLAYER_NAME, comment: "")
        let category1Stat = Double(NSLocalizedString(CATEGORY30, comment: ""))
        let category2Stat = Double(NSLocalizedString(CATEGORY31, comment: ""))
    
        let player = Player(playerName: playerName, teamName: playerTeam)
        
        switch categoryToPlayNumber {
        case 30:  player.playerCategoryStatToPlay = category1Stat; break;
        case 31:  player.playerCategoryStatToPlay = category2Stat; break;
        default: player.playerCategoryStatToPlay = 0; break;
        }
        return player
    }
    
}
