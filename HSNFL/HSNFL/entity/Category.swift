//
//  Category.swift
//  HSNBA
//
//  Created by leobs on 09.03.18.
//  Copyright © 2018 leobs. All rights reserved.
//

import Foundation

class Category{
    
    var categoryId: Int?
    var categoryName: String?
    var categoryWorldScore : Int?
    var categoryUserScore : Int?
    
    init(categoryId: Int, categoryName: String, categoryWorldScore: Int, categoryUserScore: Int){
        self.categoryId = categoryId
        self.categoryName = categoryName
        self.categoryWorldScore = categoryWorldScore
        self.categoryUserScore = categoryUserScore
    }
}
